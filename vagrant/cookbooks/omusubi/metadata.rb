maintainer       "Akira Uchiyama"
maintainer_email "asd.uchiyama@gmail.com"
license          "Apache 2.0"
description      ""
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.2.0"

depends          "database"
depends          "mysql"
depends          "apt"
depends          "omusubi"
